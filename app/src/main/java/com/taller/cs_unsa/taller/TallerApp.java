package com.taller.cs_unsa.taller;

import android.app.Application;

import com.facebook.appevents.AppEventsLogger;

/**
 * Created by CS-UNSA on 21/02/2017.
 */

public class TallerApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppEventsLogger.activateApp(this);
    }
}
